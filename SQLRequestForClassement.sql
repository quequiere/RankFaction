
SELECT 

FactionInfo.FactionUUID,


-- On display chaque type de calcul uniquement pour information
CASE WHEN table1.ClaimNumber IS NULL THEN 0 ELSE table1.ClaimNumber END AS ClaimNumber,
CASE WHEN table2.Power IS NULL THEN 0 ELSE table2.Power END AS POWER,
CASE WHEN table2.Money IS NULL THEN 0 ELSE table2.Money END AS Money,
CASE WHEN table4.ClaimApNumber IS NULL THEN 0 ELSE table4.ClaimApNumber END AS ClaimApNumber,
CASE WHEN table1.Richesse IS NULL THEN 0 ELSE table1.Richesse END AS Richesse,
FactionName,




-- On fait l'addition de chaque type de calcul
CASE WHEN table1.ClaimNumberPoint IS NULL THEN 0 ELSE table1.ClaimNumberPoint END +
CASE WHEN table2.PowerPoint IS NULL THEN 0 ELSE table2.PowerPoint END +
CASE WHEN table2.MoneyPoint IS NULL THEN 0 ELSE table2.MoneyPoint END +
CASE WHEN table4.ClaimApNumberPoint IS NULL THEN 0 ELSE table4.ClaimApNumberPoint END +
CASE WHEN table1.Richesse IS NULL THEN 0 ELSE table1.Richesse END AS totalPoint



FROM 
FactionInfo



LEFT JOIN

(SELECT (CASE WHEN COUNT(isAP)=0 THEN 0 
		WHEN COUNT(isAP)>196 THEN 5000 
		WHEN COUNT(isAP)>191 THEN 4000 
		WHEN COUNT(isAP)>176 THEN 3000 
		WHEN COUNT(isAP)>151 THEN 2000 
		WHEN COUNT(isAP)>126 THEN 1500 
		WHEN COUNT(isAP)>101 THEN 1000 
		WHEN COUNT(isAP)>76 THEN 800 
		WHEN COUNT(isAP)>51 THEN 500 
		WHEN COUNT(isAP)>26 THEN 200 
		WHEN COUNT(isAP)>0 THEN 100 END) AS ClaimNumberPoint,
		COUNT(isAP) AS ClaimNumber,
		SUM(richesse) AS Richesse,
       r.FactionOwnerUUID
FROM ClaimInfo r
GROUP BY r.FactionOwnerUUID)table1

ON table1.FactionOwnerUUID=FactionInfo.FactionUUID


LEFT JOIN

(SELECT (CASE WHEN COUNT(isAP)=0 THEN 0 
		WHEN COUNT(isAP)>196 THEN 5000 
		WHEN COUNT(isAP)>191 THEN 4000 
		WHEN COUNT(isAP)>176 THEN 3000 
		WHEN COUNT(isAP)>151 THEN 2000 
		WHEN COUNT(isAP)>126 THEN 1500 
		WHEN COUNT(isAP)>101 THEN 1000 
		WHEN COUNT(isAP)>76 THEN 800 
		WHEN COUNT(isAP)>51 THEN 500 
		WHEN COUNT(isAP)>26 THEN 200 
		WHEN COUNT(isAP)>0 THEN 100 END) AS ClaimApNumberPoint,
		COUNT(isAP) AS ClaimApNumber,
       r.FactionOwnerUUID
FROM ClaimInfo r
WHERE isap=TRUE
GROUP BY r.FactionOwnerUUID)table4

ON table4.FactionOwnerUUID=FactionInfo.FactionUUID




LEFT JOIN

(SELECT (CASE WHEN POWER=0 THEN 0
		WHEN POWER>196 THEN 2000 
		WHEN POWER>191 THEN 1500 
		WHEN POWER>176 THEN 1000 
		WHEN POWER>151 THEN 750 
		WHEN POWER>126 THEN 500 
		WHEN POWER>101 THEN 300 
		WHEN POWER>76 THEN 200 
		WHEN POWER>51 THEN 100 
		WHEN POWER>26 THEN 50 
		WHEN POWER>0 THEN 10 END) AS PowerPoint,
		
		POWER AS POWER,

	(CASE WHEN Money=0 THEN 0
		WHEN Money>4000001 THEN 6000
		WHEN Money>2000001 THEN 5000
		WHEN Money>1000001 THEN 4500
		WHEN Money>750001 THEN 4000
		WHEN Money>500001 THEN 3500
		WHEN Money>400001 THEN 3000
		WHEN Money>300001 THEN 2500
		WHEN Money>200001 THEN 2250
		WHEN Money>150001 THEN 2000
		WHEN Money>100001 THEN 1500
		WHEN Money>70001 THEN 1000
		WHEN Money>50001 THEN 750
		WHEN Money>25001 THEN 500
		WHEN Money>10001 THEN 250
		WHEN Money>1 THEN 100 END) AS MoneyPoint,
		
		Money AS Money,
		
		
       r.FactionUUID
FROM FactionInfo r
GROUP BY r.FactionUUID)table2

ON table2.FactionUUID=FactionInfo.FactionUUID



-- on du classement la safe et warzone
 -- WHERE FactionInfo.FactionUUID NOT LIKE "safezone"
 -- AND FactionInfo.FactionUUID NOT LIKE "warzone"

-- on trie du plus grand au plus petit
ORDER BY totalPoint DESC