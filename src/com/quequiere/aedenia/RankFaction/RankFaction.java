package com.quequiere.aedenia.RankFaction;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.massivecraft.massivecore.MassiveCore;
import com.quequiere.aedenia.RankFaction.classement.Classement;
import com.quequiere.aedenia.RankFaction.commands.classement.CommandeClassement;
import com.quequiere.aedenia.RankFaction.config.configs.ConfigurationSqlDataWriter;
import com.quequiere.aedenia.RankFaction.config.configs.ConfigurationSqlFactionScoreReader;
import com.quequiere.aedenia.RankFaction.config.configs.RichesseConfig;
import com.quequiere.aedenia.RankFaction.faction.chat.FactionChatModificator;
import com.quequiere.aedenia.RankFaction.faction.listener.BlockEventListener;
import com.quequiere.aedenia.RankFaction.faction.listener.ChangeMoneyListener;
import com.quequiere.aedenia.RankFaction.faction.listener.ClaimFactionListener;
import com.quequiere.aedenia.RankFaction.faction.listener.CreateAndRemoveFactionListener;
import com.quequiere.aedenia.RankFaction.faction.listener.PlayerFactionChange;
import com.quequiere.aedenia.RankFaction.faction.listener.PowerFactionListener;
import com.quequiere.aedenia.RankFaction.faction.zone.ChunkRichesse;
import com.quequiere.aedenia.RankFaction.sql.FactionBigDataWriter;
import com.quequiere.aedenia.RankFaction.sql.SqlConnector;

public class RankFaction extends JavaPlugin
{
	public static File folder;
	public static RankFaction rankfaction;

	// ---- Config
	public static ConfigurationSqlDataWriter<ConfigurationSqlDataWriter> dataBaseDataWriterConfig;
	public static ConfigurationSqlFactionScoreReader<ConfigurationSqlFactionScoreReader> dataBaseFactionScoreReader;
	public static RichesseConfig<RichesseConfig> richesseConfig;

	// ---- local non static
	private int launchTaskId = -1;
	private boolean differed = true;

	@Override
	public void onEnable()
	{

		RankFaction.rankfaction = this;

		dataBaseDataWriterConfig = new ConfigurationSqlDataWriter(this);
		dataBaseDataWriterConfig = dataBaseDataWriterConfig.loadFromFile();
		
		dataBaseFactionScoreReader = new ConfigurationSqlFactionScoreReader(this);
		dataBaseFactionScoreReader = dataBaseFactionScoreReader.loadFromFile();

		richesseConfig = new RichesseConfig(this);
		richesseConfig = richesseConfig.loadFromFile();

		launchTaskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable()
		{

			@Override
			public void run()
			{
				if (MassiveCore.get().isEnabled())
				{
					if (differed)
					{
						differed = false;
					}
					else
					{
						initializePlugin();
						Bukkit.getScheduler().cancelTask(launchTaskId);
					}

				}

			}
		}, 0, 20 * 5);

	}

	public void initializePlugin()
	{
		long start = System.currentTimeMillis();

		displayConsoleMessage("----- Initialize SQL  ----");
		
		try
		{
			dataBaseDataWriterConfig.setConnector(new SqlConnector(dataBaseDataWriterConfig));
			dataBaseFactionScoreReader.setConnector(new SqlConnector(dataBaseFactionScoreReader));
		}
		catch(NullPointerException e)
		{
			displayConsoleMessage("Erreur dans le chargement de la connexion sql, erreur de config ?");
		}

		
		FactionBigDataWriter.createTable();
		long stop = System.currentTimeMillis();
		long diff = stop - start;

		displayConsoleMessage("Initialization time finished in: " + diff + " ms");

		start = System.currentTimeMillis();
		FactionBigDataWriter.clearAndUpdateAllData();
		stop = System.currentTimeMillis();
		diff = stop - start;

		displayConsoleMessage("----- SQL write Ended in " + diff + " ms ----");

		getServer().getPluginManager().registerEvents(new CreateAndRemoveFactionListener(), this);
		getServer().getPluginManager().registerEvents(new ClaimFactionListener(), this);
		getServer().getPluginManager().registerEvents(new PowerFactionListener(), this);
		getServer().getPluginManager().registerEvents(new PlayerFactionChange(), this);
		getServer().getPluginManager().registerEvents(new ChangeMoneyListener(), this);
		getServer().getPluginManager().registerEvents(new BlockEventListener(), this);

		ChunkRichesse.startUpadaterThread();
		FactionChatModificator.registerChatTag();
		
		if(RankFaction.dataBaseFactionScoreReader.useIntegratedSqlCalculator)
		{
			Classement.startUpadaterThread();
		}
		
		new CommandeClassement("classement");

	}

	public static void displayConsoleMessage(String s)
	{
		System.out.println("[RankFaction Console] " + s);
	}

	public static void sendMessage(CommandSender p, String s)
	{
		p.sendMessage(ChatColor.GREEN + "[" + ChatColor.RED + "R" + ChatColor.GREEN + "ank" + ChatColor.RED + "F" + ChatColor.GREEN + "action] " + ChatColor.RESET + s);
	}
}
