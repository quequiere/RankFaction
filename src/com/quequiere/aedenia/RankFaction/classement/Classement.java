package com.quequiere.aedenia.RankFaction.classement;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import org.bukkit.Bukkit;
import com.massivecraft.factions.entity.Faction;
import com.quequiere.aedenia.RankFaction.RankFaction;
import com.quequiere.aedenia.RankFaction.sql.FactionDataScoreReader;

public class Classement
{
	public static HashMap<String,Integer> pointsMap = new HashMap<String,Integer>();
	public static HashMap<String, Integer> classementUUIDtoPosition = new HashMap<String, Integer>();
	public static HashMap<Integer, String> classementPositionToUUID = new HashMap<Integer, String>();
	
	public static int getGeneralClassement(Faction f)
	{
		if (classementUUIDtoPosition.containsKey(f.getId()))
		{
			return classementUUIDtoPosition.get(f.getId());
		}
		else
		{
			return -99;
		}
	}

	public static void updateClassement()
	{
		pointsMap.clear();
		classementUUIDtoPosition.clear();
		try
		{
			PreparedStatement statement = RankFaction.dataBaseDataWriterConfig.getConnector().prepareStatement(FactionDataScoreReader.getSqlRequestIntegrated());
			ResultSet rs = statement.executeQuery();
			
			int x = 1;
			while (rs.next())
			{
				String uuid = rs.getString("FactionUUID");
				classementUUIDtoPosition.put(uuid, x);
				pointsMap.put(uuid,  rs.getInt("totalPoint"));
				classementPositionToUUID.put(x, uuid);
				x++;
			}

		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}

	}

	public static void startUpadaterThread()
	{
		Bukkit.getScheduler().scheduleSyncRepeatingTask(RankFaction.rankfaction, new Runnable()
		{

			@Override
			public void run()
			{
				updateClassement();
			}
		}, 0, 20 * 30);
	}

}
