package com.quequiere.aedenia.RankFaction.faction.zone;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.block.Block;

import com.massivecraft.factions.entity.BoardColl;
import com.massivecraft.massivecore.ps.PS;
import com.quequiere.aedenia.RankFaction.RankFaction;
import com.quequiere.aedenia.RankFaction.faction.listener.BlockEventListener;
import com.quequiere.aedenia.RankFaction.sql.FactionDataUpdater;

public class ChunkRichesse
{
	public static double getRichesseForChunk(PS ps)
	{
		int total = 0;
		Chunk bukkitChunk = ps.asBukkitChunk();
		int minX = ps.getChunkX() * 16;
		int maxX = minX + 16;

		int minZ = ps.getChunkZ() * 16;
		int maxZ = minZ + 16;

		for (int y = 0; y < 256; y++)
		{
			for (int x = minX; x < maxX; x++)
			{
				for (int z = minZ; z < maxZ; z++)
				{

					Block b = bukkitChunk.getBlock(x, y, z);
					total += RankFaction.richesseConfig.getValueFromMaterial(b.getType());

				}
			}
		}

		return total;

	}

	public static void startUpadaterThread()
	{
		Bukkit.getScheduler().scheduleSyncRepeatingTask(RankFaction.rankfaction, new Runnable()
		{

			@Override
			public void run()
			{
				List<Chunk> toCheck = new ArrayList<Chunk>(BlockEventListener.toUpdate);
				BlockEventListener.toUpdate.clear();

				for (Chunk c : toCheck)
				{
					PS ps = PS.valueOf(c);
					boolean isAP = ApFinder.isAp(ps);
					FactionDataUpdater.updateChunk(BoardColl.get().getFactionAt(ps), ps, isAP);
				}

				//RankFaction.displayConsoleMessage("Richesse has been updated for " + toCheck.size() + " chunk !");
			}
		}, 0, 20 * 30);
	}

}
