package com.quequiere.aedenia.RankFaction.faction.zone;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.massivecraft.factions.entity.BoardColl;
import com.massivecraft.factions.entity.Faction;
import com.massivecraft.factions.entity.FactionColl;
import com.massivecraft.massivecore.ps.PS;
import com.quequiere.aedenia.RankFaction.RankFaction;

public class ApFinder
{
	
	public static boolean isAp(PS ps)
	{
		Faction warzone = FactionColl.get().getWarzone();
		Faction here = BoardColl.get().getFactionAt(ps);
		
		if(!here.equals(warzone))
		{
			for(PS arround:getArroundChunk(ps))
			{
				if(BoardColl.get().getFactionAt(arround).equals(warzone))
				{
					return true;
				}
			}
		}
		
		return false;
		
	}
	
	
	public static List<PS> listAllAp()
	{
		ArrayList<PS> finaleListe = new ArrayList<>();
		
		Faction warzone = FactionColl.get().getWarzone();
		Set<PS> warzoneChunk= BoardColl.get().getChunks(warzone);
		
		
		for(PS claimed: warzoneChunk)
		{
			for(PS current:getArroundChunk(claimed))
			{
				if(!finaleListe.contains(current)&&!warzoneChunk.contains(current))
				{
					finaleListe.add(current);
				}
			}
		}
		
		RankFaction.displayConsoleMessage("Calculated AP for "+finaleListe.size()+" registring chunk and "+finaleListe.size()+" has been founded !");
		
		
		return finaleListe;	
	}
	
	
	public static List<PS> getArroundChunk(PS chunk)
	{
		ArrayList<PS> liste = new ArrayList<>();
		
		liste.add(chunk.plusChunkCoords(-1, -1));
		liste.add(chunk.plusChunkCoords(-1, 0));
		liste.add(chunk.plusChunkCoords(-1, 1));
		
		liste.add(chunk.plusChunkCoords(0, -1));
		liste.add(chunk.plusChunkCoords(0, 1));
		
		liste.add(chunk.plusChunkCoords(1, -1));
		liste.add(chunk.plusChunkCoords(1, 0));
		liste.add(chunk.plusChunkCoords(1, 1));
		
		return liste;
		
	}
}
