package com.quequiere.aedenia.RankFaction.faction;

import java.util.ArrayList;
import java.util.List;

import com.massivecraft.factions.entity.Faction;
import com.massivecraft.factions.entity.FactionColl;
import com.massivecraft.factions.entity.MPlayer;

public class FactionTools
{
	public static ArrayList<Faction> getAllFaction()
	{
		ArrayList<Faction> temp = new ArrayList<Faction>();
		for (Faction f : FactionColl.get().getAll())
		{
			if (!f.isNone())
			{
				if ((f.getLeader() != null)||true) // <== Check d�sactiv�
				{
					temp.add(f);
				}
			}
		}
		
		return temp;
	}

	public static List<MPlayer> getPlayers(Faction f)
	{
		return  f.getMPlayers();
	}
}
