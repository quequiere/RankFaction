package com.quequiere.aedenia.RankFaction.faction.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import com.earth2me.essentials.OfflinePlayer;
import com.massivecraft.factions.entity.Faction;
import com.massivecraft.factions.entity.FactionColl;
import com.quequiere.aedenia.RankFaction.sql.FactionDataUpdater;

import net.ess3.api.events.UserBalanceUpdateEvent;

public class ChangeMoneyListener implements Listener
{
	@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
	public void onCreate(UserBalanceUpdateEvent e)
	{
		if(e.getPlayer() instanceof OfflinePlayer)
		{
			
			String parsedId = e.getPlayer().getName().split("faction_")[1];
			parsedId=parsedId.replace("_", "-");

			if (FactionColl.get().containsId(parsedId))
			{
				Faction faction = FactionColl.get().get(parsedId);
				FactionDataUpdater.factionChangeMoney(faction, e.getNewBalance());
			}
		
		}
		
		
	}
}
