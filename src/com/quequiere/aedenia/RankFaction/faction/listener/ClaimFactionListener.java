package com.quequiere.aedenia.RankFaction.faction.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import com.massivecraft.factions.entity.FactionColl;
import com.massivecraft.factions.event.EventFactionsChunksChange;
import com.massivecraft.massivecore.ps.PS;
import com.quequiere.aedenia.RankFaction.RankFaction;
import com.quequiere.aedenia.RankFaction.faction.zone.ApFinder;
import com.quequiere.aedenia.RankFaction.sql.FactionDataUpdater;

public class ClaimFactionListener implements Listener
{
	@EventHandler(priority = EventPriority.NORMAL)
	public void onCreate(EventFactionsChunksChange e)
	{
		
		for (PS ps : e.getChunks())
		{
			if(ps==null)
				continue;
			boolean isAP = ApFinder.isAp(ps);
			
			if(isAP&&!e.getNewFaction().equals(FactionColl.get().getWarzone()))
			{
				
				int nombreAp = FactionDataUpdater.getFactionAp(e.getNewFaction());
				if(nombreAp>=5)
				{
					e.setCancelled(true);
					RankFaction.sendMessage(e.getMSender().getPlayer(), "Vous avez d�j� claim le maximum d'AP !");
				}
				else 
				{
					RankFaction.sendMessage(e.getMSender().getPlayer(), "Vous avez claim un chunk AP ! ("+nombreAp+"/5)");
				}
				
				
				
			}
			
			
			FactionDataUpdater.updateChunk(e.getNewFaction(),ps,isAP);
			
			
		}
		
		
	}
	
}
