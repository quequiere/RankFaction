package com.quequiere.aedenia.RankFaction.faction.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import com.massivecraft.factions.entity.Faction;
import com.massivecraft.factions.event.EventFactionsPowerChange;
import com.quequiere.aedenia.RankFaction.sql.FactionDataUpdater;

public class PowerFactionListener implements Listener
{
	@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
	public void onCreate(EventFactionsPowerChange e)
	{
		//RankFaction.displayConsoleMessage("Update power to database !");
		if(e.getMPlayer().hasFaction())
		{
			Faction f = e.getMPlayer().getFaction();
			FactionDataUpdater.updatePower(f, f.getPower());
		}
		
	}
}
