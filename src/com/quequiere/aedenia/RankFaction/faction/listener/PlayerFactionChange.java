package com.quequiere.aedenia.RankFaction.faction.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import com.massivecraft.factions.entity.FactionColl;
import com.massivecraft.factions.event.EventFactionsMembershipChange;
import com.massivecraft.factions.event.EventFactionsMembershipChange.MembershipChangeReason;
import com.quequiere.aedenia.RankFaction.RankFaction;
import com.quequiere.aedenia.RankFaction.sql.FactionDataUpdater;

public class PlayerFactionChange implements Listener
{
	@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
	public void onCreate(EventFactionsMembershipChange e)
	{
		RankFaction.displayConsoleMessage("Change player data to database !"); 
		if(e.getReason().equals(MembershipChangeReason.LEAVE))
		{
			
			FactionDataUpdater.playerChangeFaction(e.getMPlayer(),FactionColl.get().getNone());
		}
		else
		{
			FactionDataUpdater.playerChangeFaction(e.getMPlayer(),e.getNewFaction());
		}
		
		
		
	}
}
