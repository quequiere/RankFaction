package com.quequiere.aedenia.RankFaction.faction.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import com.massivecraft.factions.event.EventFactionsCreate;
import com.massivecraft.factions.event.EventFactionsDisband;
import com.quequiere.aedenia.RankFaction.RankFaction;
import com.quequiere.aedenia.RankFaction.sql.FactionDataUpdater;

public class CreateAndRemoveFactionListener implements Listener
{
	@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
	public void onCreate(EventFactionsCreate e)
	{
		RankFaction.displayConsoleMessage("Creating new faction to database !");
		FactionDataUpdater.createFaction(e.getFactionId(),e.getFactionName());
	}
	
	@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
	public void onRemove(EventFactionsDisband e)
	{
		RankFaction.displayConsoleMessage("Remove faction from database !");
		FactionDataUpdater.removeFaction(e.getFaction());
	}
}
