package com.quequiere.aedenia.RankFaction.faction.listener;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Chunk;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

import com.massivecraft.factions.entity.BoardColl;
import com.massivecraft.factions.entity.Faction;
import com.massivecraft.massivecore.ps.PS;
import com.quequiere.aedenia.RankFaction.RankFaction;

public class BlockEventListener implements Listener
{

	public static List<Chunk> toUpdate = new ArrayList<Chunk>();

	@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
	public void onCreate(BlockBreakEvent e)
	{
		Faction f = BoardColl.get().getFactionAt(PS.valueOf(e.getBlock().getChunk()));
		if (f.isNormal() && !toUpdate.contains(e.getBlock().getChunk())&&RankFaction.richesseConfig.hasValueForMaterial(e.getBlock().getType()))
		{
			toUpdate.add(e.getBlock().getChunk());
		}

	}

	@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
	public void onCreate(BlockPlaceEvent e)
	{

		Faction f = BoardColl.get().getFactionAt(PS.valueOf(e.getBlock().getChunk()));
		if (f.isNormal() && !toUpdate.contains(e.getBlock().getChunk())&&RankFaction.richesseConfig.hasValueForMaterial(e.getBlock().getType()))
		{
			toUpdate.add(e.getBlock().getChunk());
		}

	}
}
