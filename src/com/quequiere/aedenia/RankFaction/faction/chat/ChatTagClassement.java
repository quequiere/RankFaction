package com.quequiere.aedenia.RankFaction.faction.chat;

import org.bukkit.command.CommandSender;

import com.massivecraft.factions.chat.ChatTagAbstract;
import com.massivecraft.factions.entity.Faction;
import com.massivecraft.factions.entity.MPlayer;
import com.quequiere.aedenia.RankFaction.sql.FactionDataScoreReader;

public class ChatTagClassement extends ChatTagAbstract
{
	private static ChatTagClassement i = new ChatTagClassement();

	public ChatTagClassement()
	{
		super("classement_rankfaction");
	}

	public static ChatTagClassement get()
	{
		return i;
	}

	public String getReplacement(CommandSender sender, CommandSender recipient)
	{
		MPlayer usender = MPlayer.get(sender);

		Faction faction = usender.getFaction();
		if (faction.isNone())
		{
			return "";
		}
		
		
		return String.valueOf(FactionDataScoreReader.getFactionClassement(faction));
	}

}
