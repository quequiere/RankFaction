package com.quequiere.aedenia.RankFaction.commandsTools;

import org.bukkit.command.CommandSender;

public interface ISubCommand {

	public void execute(String[] args, CommandSender sender);

	public String getBase();

	public String getDescription();

}
