package com.quequiere.aedenia.RankFaction.commandsTools;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import com.quequiere.aedenia.RankFaction.RankFaction;

public abstract class BaseCommand implements CommandExecutor
{
	public static ArrayList<BaseCommand> commands = new ArrayList<BaseCommand>();

	private String name;


	public BaseCommand(String name)
	{
		this.name = name;
		commands.add(this);
		RankFaction.rankfaction.getCommand(this.name).setExecutor(this);
	}

	public String getCommandUsage()
	{
		final StringBuilder s = new StringBuilder();
		s.append(ChatColor.BOLD+"Commandes:");
		for (final ISubCommand c : this.getSubCommands())
		{
			s.append("\n");
			s.append(ChatColor.GREEN+"/"+this.name+" "+ChatColor.RED+c.getBase()+ChatColor.GRAY+" - "+c.getDescription());
		}
		return s.toString();
	}

	public abstract ISubCommand[] getSubCommands();

	@Override
	public boolean onCommand(CommandSender s, Command command, String base, String[] args)
	{

		if (args.length == 0)
		{
			s.sendMessage(getCommandUsage());
			return true;
		}
		
		String firstArg = args[0];
		for (final ISubCommand c : getSubCommands())
		{
			if (c.getBase().equals(firstArg))
			{
				c.execute(args, s);
				return true;
			}
		}

		s.sendMessage(getCommandUsage());
		return true;

	}

}
