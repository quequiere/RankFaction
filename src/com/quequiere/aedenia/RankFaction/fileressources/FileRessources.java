package com.quequiere.aedenia.RankFaction.fileressources;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public class FileRessources
{
	static public String ExportResource(String resourceName, File folder) throws Exception
	{
		InputStream stream = null;
		OutputStream resStreamOut = null;

		try
		{

			stream = FileRessources.class.getResourceAsStream(resourceName);
			if (stream == null)
			{
				throw new Exception("Cannot get resource \"" + resourceName + "\" from Jar file.");
			}

			int readBytes;
			byte[] buffer = new byte[4096];

			resStreamOut = new FileOutputStream(folder.getAbsolutePath() + resourceName);
			while ((readBytes = stream.read(buffer)) > 0)
			{
				resStreamOut.write(buffer, 0, readBytes);
			}

		}
		catch (Exception ex)
		{
			throw ex;
		}
		finally
		{
			stream.close();
			resStreamOut.close();
		}

		System.out.println("Finish writing: " + folder.getAbsolutePath());

		return folder.getAbsolutePath() + resourceName;
	}
}
