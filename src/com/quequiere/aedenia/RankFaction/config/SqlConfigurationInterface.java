package com.quequiere.aedenia.RankFaction.config;

import com.quequiere.aedenia.RankFaction.sql.SqlConnector;

public interface SqlConfigurationInterface
{
	public String getHost();
	public int getPort();
	public String getDatabase();
	public String getUser();
	public String getPassword();
	public SqlConnector getConnector();
	public void setConnector(SqlConnector sqlconnector);
	
}
