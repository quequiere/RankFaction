package com.quequiere.aedenia.RankFaction.config.configs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.bukkit.plugin.java.JavaPlugin;

import com.quequiere.aedenia.RankFaction.RankFaction;
import com.quequiere.aedenia.RankFaction.config.SavebleJsonObject;
import com.quequiere.aedenia.RankFaction.config.SqlConfigurationInterface;
import com.quequiere.aedenia.RankFaction.fileressources.FileRessources;
import com.quequiere.aedenia.RankFaction.sql.FactionDataScoreReader;
import com.quequiere.aedenia.RankFaction.sql.SqlConnector;

public class ConfigurationSqlFactionScoreReader<T> extends SavebleJsonObject<T> implements SqlConfigurationInterface
{
	private String host = "host.fr", database = "database", username = "user", password = "pass";
	public String tableName = "table";
	public String fieldToGetName = "fieldToGetName";
	public String factionUUIDFieldName = "factionUUIDFieldName";
	private int port = 3306;
	public boolean useIntegratedSqlCalculator = false;

	private SqlConnector sqlDataWriterConnector;

	public ConfigurationSqlFactionScoreReader(JavaPlugin plugin)
	{
		super(plugin, "configuration/", "ConfigurationSqlFactionScoreReader.json");
	}

	@Override
	public String getHost()
	{
		return host;
	}

	@Override
	public int getPort()
	{
		return port;
	}

	@Override
	public String getDatabase()
	{
		return database;
	}

	@Override
	public String getUser()
	{
		return username;
	}

	@Override
	public String getPassword()
	{
		return password;
	}

	@Override
	public SqlConnector getConnector()
	{
		return sqlDataWriterConnector;
	}

	@Override
	public void setConnector(SqlConnector sqlconnector)
	{
		this.sqlDataWriterConnector = sqlconnector;
	}

	@Override
	public T loadFromFile()
	{
		this.reloadSQLFile();
		return super.loadFromFile();
	}

	public void reloadSQLFile()
	{
		String fileNameS = "SQLRequestForClassement.sql";
		File folder = new File("./plugins/" + RankFaction.rankfaction.getName() + "/" + this.folders);
		folder.mkdirs();
		File f = new File(folder.getAbsolutePath() + "/" + fileNameS);

		if (!f.exists())
		{
			try
			{
				FileRessources.ExportResource("/"+fileNameS,folder);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		
		
		BufferedReader br = null;
		try
		{
			br = new BufferedReader(new FileReader(f));
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while (line != null)
			{
				sb.append(line);
				sb.append(System.lineSeparator());
				line = br.readLine();
			}
			String everything = sb.toString();
			FactionDataScoreReader.SQLRequestIntegrated=everything;
		
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				br.close();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}

	}
}
