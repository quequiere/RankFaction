package com.quequiere.aedenia.RankFaction.config.configs;

import java.util.HashMap;

import org.bukkit.Material;
import org.bukkit.plugin.java.JavaPlugin;

import com.quequiere.aedenia.RankFaction.config.SavebleJsonObject;

public class RichesseConfig<T> extends SavebleJsonObject<T>
{
	private HashMap<String, Double> values = new HashMap<String, Double>();
	
	public RichesseConfig(JavaPlugin plugin)
	{
		super(plugin, "configuration/", "RichesseConfig.json");
		
		//en cas d initliazation par instanciation et non par json, et donc si premiere fois par consequence
		
		for(Material m:Material.values())
		{
			if(m.isBlock())
			{
				values.put(m.name(), 0.0);
			}
			
		}
		
	}
	
	public double getValueFromMaterial(Material m)
	{
		if(values.containsKey(m.name()))
		{
			return values.get(m.name());
		}
		else
		{
			return 0;
		}
	}
	
	public boolean hasValueForMaterial(Material m)
	{
		if(values.containsKey(m.name()))
		{
			return true;
		}
		return false;
	}



	
}
