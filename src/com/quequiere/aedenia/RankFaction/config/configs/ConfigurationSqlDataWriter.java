package com.quequiere.aedenia.RankFaction.config.configs;

import org.bukkit.plugin.java.JavaPlugin;

import com.quequiere.aedenia.RankFaction.config.SavebleJsonObject;
import com.quequiere.aedenia.RankFaction.config.SqlConfigurationInterface;
import com.quequiere.aedenia.RankFaction.sql.SqlConnector;

public class ConfigurationSqlDataWriter<T> extends SavebleJsonObject<T> implements SqlConfigurationInterface
{
	private String host="host.fr", database="database", username="user", password="pass";
	private int port=3306;
	
	private SqlConnector sqlDataWriterConnector;
	
	public ConfigurationSqlDataWriter(JavaPlugin plugin)
	{
		super(plugin, "configuration/", "ConfigurationSqlEcritureData.json");
	}

	@Override
	public String getHost()
	{
		return host;
	}

	@Override
	public int getPort()
	{
		return port;
	}

	@Override
	public String getDatabase()
	{
		return database;
	}

	@Override
	public String getUser()
	{
		return username;
	}

	@Override
	public String getPassword()
	{
		return password;
	}

	@Override
	public SqlConnector getConnector()
	{
		return sqlDataWriterConnector;
	}

	@Override
	public void setConnector(SqlConnector sqlconnector)
	{
		this.sqlDataWriterConnector=sqlconnector;	
	}


	
}
