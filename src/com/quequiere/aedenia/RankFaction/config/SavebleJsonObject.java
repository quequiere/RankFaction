package com.quequiere.aedenia.RankFaction.config;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.quequiere.aedenia.RankFaction.RankFaction;

import net.minecraft.util.com.google.gson.Gson;
import net.minecraft.util.com.google.gson.GsonBuilder;
import net.minecraft.util.com.google.gson.JsonSyntaxException;

public abstract class SavebleJsonObject<T>
{
	public static File baseFolder;
	
	protected transient String folders;
	private transient String fileName;
	private transient Plugin plugin;
	
	public SavebleJsonObject(JavaPlugin plugin,String folders,String filename)
	{
		SavebleJsonObject.baseFolder = new File("./plugins/"+plugin.getName()+"/");
		SavebleJsonObject.baseFolder.mkdirs();
		
		
		this.folders=folders;
		this.fileName=filename;
		this.plugin=plugin;
	}
	 
	private String toJson()
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.toJson(this);
	}

	private T fromJson(String s)
	{
		try
		{
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			return (T) gson.fromJson(s, this.getClass());
		}
		catch(JsonSyntaxException e)
		{
			RankFaction.displayConsoleMessage("ERREUR ! Le format d'une configuration est fausse, merci de verifier la syntaxe pour: "+this.folders+this.fileName);
		}
		return null;
	
	}
	
	public void save()
	{
		File folder = new File("./plugins/"+plugin.getName()+"/"+this.folders);
		Writer writer = null;
		try
		{

			folder.mkdirs();
			File file = new File(folder.getAbsolutePath() +"/"+ this.fileName);
			if (!file.exists())
			{
				file.createNewFile();
			}

			writer = new OutputStreamWriter(new FileOutputStream(file));
			writer.write(this.toJson());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				writer.close();
			}
			catch (Exception e)
			{
			}
		}

	}

	public T loadFromFile()
	{
		File folder = new File("./plugins/"+plugin.getName()+"/"+this.folders);
		folder.mkdirs();
		File f = new File(folder.getAbsolutePath() + "/"+this.fileName);
		
		if (f.exists())
		{
			BufferedReader br = null;
			try
			{
				br = new BufferedReader(new FileReader(f));
				StringBuilder sb = new StringBuilder();
				String line = br.readLine();

				while (line != null)
				{
					sb.append(line);
					sb.append(System.lineSeparator());
					line = br.readLine();
				}
				String everything = sb.toString();
				T d = this.fromJson(everything);
				if (d != null)
				{
					return d;
				}
				else
				{
					System.out.println("Fail load: " + f.getAbsolutePath());
				}
			}
			catch (FileNotFoundException e)
			{
				e.printStackTrace();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
			finally
			{
				try
				{
					br.close();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}
		else
		{

			System.out.println("Cant find: " + f.getAbsolutePath()+" creating new one !");
			this.save();
			return (T) this;
		}

		return null;
	}
}
