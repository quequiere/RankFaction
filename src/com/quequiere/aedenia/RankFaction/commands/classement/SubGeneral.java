package com.quequiere.aedenia.RankFaction.commands.classement;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import com.massivecraft.factions.entity.Faction;
import com.massivecraft.factions.entity.FactionColl;
import com.quequiere.aedenia.RankFaction.RankFaction;
import com.quequiere.aedenia.RankFaction.classement.Classement;
import com.quequiere.aedenia.RankFaction.commandsTools.ISubCommand;

public class SubGeneral implements ISubCommand
{

	public static int pageSize = 5;

	@Override
	public void execute(String[] args, CommandSender sender)
	{

		int totalPage = (int) Math.floor(Classement.classementUUIDtoPosition.size() / pageSize);

		if (args.length == 1)
		{
			displayClassement(sender, 1, totalPage + 1);
		}
		else
		{
			try
			{
				int page = Integer.parseInt(args[1]);

				if (page > totalPage + 1)
				{
					displayClassement(sender, totalPage + 1, totalPage + 1);
				}
				else
				{
					displayClassement(sender, page, totalPage + 1);
				}

			}
			catch (NumberFormatException e)
			{
				RankFaction.sendMessage(sender, "Vous devez utiliser un chiffre comme argument !");
			}
		}

	}

	public void displayClassement(CommandSender s, int page, int total)
	{
		s.sendMessage(ChatColor.BOLD + "" + ChatColor.DARK_GREEN + "============ Classement G�n�ral ============");
		s.sendMessage(ChatColor.GOLD + "                           Page " + page + "/" + total);

		int debut = (page - 1) * pageSize + 1;
		int fin = debut + pageSize - 1;

		for (int x = debut; x - 1 < Classement.classementPositionToUUID.size() && x <= fin; x++)
		{
			String uuid = Classement.classementPositionToUUID.get(x);
			int position = Classement.classementUUIDtoPosition.get(uuid);
			int point = Classement.pointsMap.get(uuid);
			Faction f = FactionColl.get().get(uuid);
			s.sendMessage(ChatColor.BOLD + "" + ChatColor.RED + position + ChatColor.RESET + ChatColor.GRAY + " - " + ChatColor.GREEN + "" + f.getName() + " " + ChatColor.RESET + ChatColor.GRAY
					+ ChatColor.ITALIC + "(" + point + ")");
		}

		s.sendMessage(ChatColor.BOLD + "" + ChatColor.DARK_GREEN + "==========================================");
	}

	@Override
	public String getBase()
	{
		return "general";
	}

	@Override
	public String getDescription()
	{
		return "Affiche le classement g�n�ral";
	}

}
