package com.quequiere.aedenia.RankFaction.commands.classement;

import com.quequiere.aedenia.RankFaction.commandsTools.BaseCommand;
import com.quequiere.aedenia.RankFaction.commandsTools.ISubCommand;

public class CommandeClassement extends BaseCommand
{

	public CommandeClassement(String name)
	{
		super(name);
	}

	@Override
	public ISubCommand[] getSubCommands()
	{
		 ISubCommand[] commands = {new SubGeneral()};
		 return commands;
	}

}
