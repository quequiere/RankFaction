package com.quequiere.aedenia.RankFaction.sql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.massivecraft.factions.entity.Faction;
import com.quequiere.aedenia.RankFaction.RankFaction;
import com.quequiere.aedenia.RankFaction.classement.Classement;

public class FactionDataScoreReader
{
	public static String SQLRequestIntegrated ="";

	public static int getFactionClassement(Faction faction)
	{
		if(!RankFaction.dataBaseFactionScoreReader.useIntegratedSqlCalculator)
		{
			try
			{
				PreparedStatement statement = RankFaction.dataBaseDataWriterConfig.getConnector().prepareStatement("SELECT " + RankFaction.dataBaseFactionScoreReader.fieldToGetName + " FROM "
						+ RankFaction.dataBaseFactionScoreReader.tableName + " WHERE " + RankFaction.dataBaseFactionScoreReader.factionUUIDFieldName + "=?");

				statement.setString(1, faction.getId());
				ResultSet rs = statement.executeQuery();
				if (rs.next())
				{
					int value = rs.getInt(1);
					return value;
				}

			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
		else
		{
			return Classement.getGeneralClassement(faction);
		}
	

		return -1;
	}

	
	public static String getSqlRequestIntegrated()
	{
		if(SQLRequestIntegrated==null||SQLRequestIntegrated.equals(""))
		{
			RankFaction.dataBaseFactionScoreReader.reloadSQLFile();
		}
		
		return SQLRequestIntegrated;
	}
}
