package com.quequiere.aedenia.RankFaction.sql;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.massivecraft.factions.entity.Board;
import com.massivecraft.factions.entity.BoardColl;
import com.massivecraft.factions.entity.Faction;
import com.massivecraft.factions.entity.MPlayer;
import com.massivecraft.factions.entity.MPlayerColl;
import com.massivecraft.massivecore.money.Money;
import com.massivecraft.massivecore.ps.PS;
import com.quequiere.aedenia.RankFaction.RankFaction;
import com.quequiere.aedenia.RankFaction.faction.FactionTools;
import com.quequiere.aedenia.RankFaction.faction.zone.ApFinder;
import com.quequiere.aedenia.RankFaction.faction.zone.ChunkRichesse;

public class FactionBigDataWriter
{
	public static void clearAndUpdateAllData()
	{
		RankFaction.displayConsoleMessage("Removing old data ...");

		try
		{
			Statement statement = RankFaction.dataBaseDataWriterConfig.getConnector().getStatement();
			statement.executeUpdate("DELETE from `FactionInfo`");
			statement.executeUpdate("DELETE from `PlayerInfo`");
			statement.executeUpdate("DELETE from `ClaimInfo`");
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		writeAllFaction();
		writeAllPlayer();
		writeAllChunk();
	}
	


	private static void writeAllChunk()
	{
		Collection<Board> liste = BoardColl.get().getAll();
		RankFaction.displayConsoleMessage("Board to write, writing all chunk: " + liste.size());
		int i = 0;
		
		//mieu vaut lister tous les ap en premier que de chercher autout de tous les chunks de tous les claims
		List<PS> apListe = ApFinder.listAllAp();

		for (Board b : liste)
		{
			Map<Faction, Set<PS>> map = b.getFactionToChunks();
			for (Map.Entry entry : map.entrySet())
			{
				Faction faction = (Faction) entry.getKey();
				Set<PS> chunkList = map.get(faction);

				for (Object object : chunkList.toArray())
				{
					PS ps = (PS) object;
					try
					{
						PreparedStatement statement = RankFaction.dataBaseDataWriterConfig.getConnector().prepareStatement("INSERT INTO ClaimInfo (FactionOwnerUUID,Location,isAp,richesse) values(?,?,?,?)");
						statement.setString(1, faction.getId());
						statement.setString(2, ps.toString());
						statement.setBoolean(3, apListe.contains(ps)?true:false);
						statement.setDouble(4, ChunkRichesse.getRichesseForChunk(ps));
						statement.executeUpdate();

					}
					catch (SQLException e)
					{
						e.printStackTrace();
					}

					i++;

				}

			}

		}

		RankFaction.displayConsoleMessage("Chunk writed: " + i);

	}

	private static void writeAllPlayer()
	{
		Collection<MPlayer> liste = MPlayerColl.get().getAll();
		RankFaction.displayConsoleMessage("Player to write: " + liste.size());
		for (MPlayer p : liste)
		{
			if(p.getUuid()==null)
			{
				continue;
			}
			
			String factionId = "none";
			if (p.getFaction() != null)
			{
				factionId = p.getFaction().getId();
			}

			try
			{
				PreparedStatement statement = RankFaction.dataBaseDataWriterConfig.getConnector().prepareStatement("INSERT INTO PlayerInfo (PlayerUUID,PlayerName,FactionUUID) values(?,?,?)");
				statement.setString(1, p.getUuid().toString());
				statement.setString(2, p.getName());
				statement.setString(3, factionId);
				statement.executeUpdate();

			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
	}

	private static void writeAllFaction()
	{
		ArrayList<Faction> liste = FactionTools.getAllFaction();
		RankFaction.displayConsoleMessage("Faction to write: " + liste.size());
		for (Faction f : liste)
		{
			try
			{
				PreparedStatement statement = RankFaction.dataBaseDataWriterConfig.getConnector().prepareStatement("INSERT INTO FactionInfo (FactionUUID,FactionName,power,Money) values(?,?,?,?)");
				statement.setString(1, f.getId());
				statement.setString(2, f.getName());
				statement.setDouble(3, f.getPower());
				statement.setBigDecimal(4, new BigDecimal(Money.get(f)));
				statement.executeUpdate();

			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public static void createTable()
	{
		RankFaction.displayConsoleMessage("Checking table creation");
		
		try
		{
			Statement statement = RankFaction.dataBaseDataWriterConfig.getConnector().getStatement();
			statement.executeUpdate("CREATE TABLE IF NOT EXISTS `FactionInfo` (`FactionUUID` varchar(200) NOT NULL UNIQUE, `FactionName` varchar(200),`power` DOUBLE, `Money` DECIMAL)");
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		
		try
		{
			Statement statement =RankFaction.dataBaseDataWriterConfig.getConnector().getStatement();
			statement.executeUpdate("CREATE TABLE IF NOT EXISTS `PlayerInfo` (`PlayerUUID` varchar(200) NOT NULL UNIQUE, `PlayerName` varchar(200),`FactionUUID` varchar(200))");
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		
		try
		{
			Statement statement = RankFaction.dataBaseDataWriterConfig.getConnector().getStatement();
			statement.executeUpdate("CREATE TABLE IF NOT EXISTS `ClaimInfo` (`FactionOwnerUUID` varchar(200),`Location` varchar(200) NOT NULL UNIQUE, `isAp` BOOLEAN DEFAULT 0,`richesse` DOUBLE)");
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		
		
		RankFaction.displayConsoleMessage("Checked table and created if needed !");
	}
}
