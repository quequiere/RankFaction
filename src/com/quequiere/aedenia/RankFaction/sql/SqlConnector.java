package com.quequiere.aedenia.RankFaction.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import com.quequiere.aedenia.RankFaction.RankFaction;
import com.quequiere.aedenia.RankFaction.config.SqlConfigurationInterface;

public class SqlConnector
{
	private Connection connection;
	private SqlConfigurationInterface configToUse;

	public SqlConnector(SqlConfigurationInterface config)
	{
		this.configToUse = config;
	}

	private void openConnection() throws SQLException, ClassNotFoundException
	{
		if (connection != null && !connection.isClosed())
		{
			return;
		}

		synchronized (this)
		{
			if (connection != null && !connection.isClosed())
			{
				return;
			}
			Class.forName("com.mysql.jdbc.Driver");
			connection = this.createConnection();
		}
	}

	public PreparedStatement prepareStatement(String request)
	{
		try
		{
			this.openConnection();
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		try
		{
			return this.connection.prepareStatement(request);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		return null;
	}

	public Statement getStatement()
	{
		try
		{
			this.openConnection();
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		try
		{
			return this.connection.createStatement();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		return null;
	}

	public Connection createConnection()
	{
		try
		{
			return DriverManager.getConnection("jdbc:mysql://" + this.configToUse.getHost() + ":" + this.configToUse.getPort() + "/" + this.configToUse.getDatabase(), this.configToUse.getUser(), this.configToUse.getPassword());
		}
		catch (SQLException e)
		{
			RankFaction.displayConsoleMessage("Impossible de cr�er la connexion avec le serveur SQL !");
		}
		return null;
	}

}
