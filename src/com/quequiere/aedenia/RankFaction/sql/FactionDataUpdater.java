package com.quequiere.aedenia.RankFaction.sql;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.massivecraft.factions.entity.Faction;
import com.massivecraft.factions.entity.MPlayer;
import com.massivecraft.massivecore.ps.PS;
import com.quequiere.aedenia.RankFaction.RankFaction;
import com.quequiere.aedenia.RankFaction.classement.Classement;
import com.quequiere.aedenia.RankFaction.faction.zone.ChunkRichesse;

public class FactionDataUpdater
{

	public static void factionChangeMoney(Faction faction, BigDecimal bigDecimal)
	{
		try
		{
			PreparedStatement statement = RankFaction.dataBaseDataWriterConfig.getConnector().prepareStatement("UPDATE FactionInfo SET Money=? WHERE FactionUUID=?");

			// insert part
			statement.setBigDecimal(1, bigDecimal);
			statement.setString(2, faction.getId());
			statement.executeUpdate();

		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public static void playerChangeFaction(MPlayer mPlayer, Faction faction)
	{
		try
		{

			PreparedStatement statement = RankFaction.dataBaseDataWriterConfig.getConnector()
					.prepareStatement("INSERT INTO PlayerInfo (PlayerUUID, PlayerName, FactionUUID) VALUES(?, ?, ?) ON DUPLICATE KEY UPDATE FactionUUID=? , PlayerName=?  ");

			// insert part
			statement.setString(1, mPlayer.getUuid().toString());
			statement.setString(2, mPlayer.getName());
			statement.setString(3, faction.getId());

			// update part
			statement.setString(4, faction.getId());
			statement.setString(5, mPlayer.getName()); // Au cas ou changement
														// nom mojang
			statement.executeUpdate();

		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}

	}

	public static void createFaction(String id, String name)
	{
		try
		{
			PreparedStatement statement = RankFaction.dataBaseDataWriterConfig.getConnector().prepareStatement("INSERT INTO FactionInfo (FactionUUID,FactionName,power,Money) values(?,?,?,?)");
			statement.setString(1, id);
			statement.setString(2, name);
			statement.setDouble(3, 0);
			statement.setBigDecimal(4, new BigDecimal(0));
			statement.executeUpdate();

		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public static void removeFaction(Faction f)
	{
		try
		{
			PreparedStatement statement = RankFaction.dataBaseDataWriterConfig.getConnector().prepareStatement("DELETE FROM FactionInfo WHERE FactionUUID=?");
			statement.setString(1, f.getId());
			statement.executeUpdate();

		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}

	}

	public static void updatePower(Faction faction, double newPower)
	{
		try
		{
			PreparedStatement statement = RankFaction.dataBaseDataWriterConfig.getConnector().prepareStatement("UPDATE FactionInfo SET power=? WHERE FactionUUID=?");

			// insert part
			statement.setDouble(1, newPower);
			statement.setString(2, faction.getId());
			statement.executeUpdate();

		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public static void updateChunk(Faction faction, PS ps, boolean isAP)
	{

		double richesse = ChunkRichesse.getRichesseForChunk(ps);
		try
		{

			PreparedStatement statement = RankFaction.dataBaseDataWriterConfig.getConnector()
					.prepareStatement("INSERT INTO ClaimInfo (FactionOwnerUUID, Location, isAp,richesse) VALUES(?, ?, ?,?) ON DUPLICATE KEY UPDATE FactionOwnerUUID=? , isAp=?, richesse=?  ");

			// insert part
			statement.setString(1, faction.getId());
			statement.setString(2, ps.toString());
			statement.setBoolean(3, isAP);
			statement.setDouble(4, richesse);

			// update part
			statement.setString(5, faction.getId());
			statement.setBoolean(6, isAP);
			statement.setDouble(7, richesse);
			statement.executeUpdate();

		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}

	}

	public static int getFactionAp(Faction faction)
	{

		try
		{
			PreparedStatement statement = RankFaction.dataBaseDataWriterConfig.getConnector()
					.prepareStatement("SELECT count(*) FROM ClaimInfo WHERE FactionOwnerUUID=? and isAp=True");

			statement.setString(1, faction.getId());
			ResultSet rs = statement.executeQuery();
			if (rs.next())
			{
				int value = rs.getInt(1);
				return value;
			}

		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		return -1;
	}

}
