
<?php
session_start();
header('Content-type: text/html; charset=utf-8');

//---------------------------------------------------------
// Requires fichiers database
//---------------------------------------------------------
require_once('../configuration/configuration.php');
require_once('../configuration/configbravoure.php');
require_once('../configuration/baseDonnees.php');
require_once('../configuration/fonctions.php');

$titre = 'Classement faction';
require_once("../jointures/head.php");
require_once("../jointures/header.php");
?>
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet'  type='text/css'>
<style>
    /* Classe obligatoire pour les flèches */
    .flecheDesc {
        width: 0;
        height: 0;
        float:right;
        margin: 10px;
        border-left: 5px solid transparent;
        border-right: 5px solid transparent;
        border-bottom: 5px solid black;
    }
    .flecheAsc {
        width: 0;
        height: 0;
        float:right;
        margin: 10px;
        border-left: 5px solid transparent;
        border-right: 5px solid transparent;
        border-top: 5px solid black;
    }

    /* Classe optionnelle pour le style */



    .avectri th {text-align:center;padding:5px 0 0 5px;vertical-align: middle;background-color:#999690;color:#444;cursor:pointer;
        -webkit-touch-callout: none;
        -webkit-user-select: none;
        -khtml-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        -o-user-select: none;
        user-select: none;
    }

    .avectri th.selection {background-color:#5d625c;color: #e1741b;}
    .avectri th.selection .flecheDesc {border-bottom-color: #e1741b;}
    .avectri th.selection .flecheAsc {border-top-color: #e1741b;}


</style>

<section class="content-block default-bg">

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="http://getbootstrap.com/dist/js/bootstrap.min.js"></script>
    <script>
        // Tri dynamique de tableau HTML
        // Auteur : Copyright © 2015 - Django Blais
        // Source : http://trucsweb.com/tutoriels/Javascript/tableau-tri/
        // Sous licence du MIT.
        function twInitTableau() {
            // Initialise chaque tableau de classe « avectri »
            [].forEach.call( document.getElementsByClassName("avectri"), function(oTableau) {
                var oEntete = oTableau.getElementsByTagName("tr")[0];
                var nI = 1;
                // Ajoute à chaque entête (th) la capture du clic
                // Un picto flèche, et deux variable data-*
                // - Le sens du tri (0 ou 1)
                // - Le numéro de la colonne
                [].forEach.call( oEntete.querySelectorAll("th"), function(oTh) {
                    oTh.addEventListener("click", twTriTableau, false);
                    oTh.setAttribute("data-pos", nI);
                    if(oTh.getAttribute("data-tri")=="1") {
                        oTh.innerHTML += "<span class=\"flecheDesc\"></span>";
                    } else {
                        oTh.setAttribute("data-tri", "0");
                        oTh.innerHTML += "<span class=\"flecheAsc\"></span>";
                    }
                    // Tri par défaut
                    if (oTh.className=="selection") {
                        oTh.click();
                    }
                    nI++;
                });
            });
        }

        function twInit() {
            twInitTableau();
        }
        function twPret(maFonction) {
            if (document.readyState != "loading"){
                maFonction();
            } else {
                document.addEventListener("DOMContentLoaded", maFonction);
            }
        }
        twPret(twInit);

        function twTriTableau() {
            // Ajuste le tri
            var nBoolDir = this.getAttribute("data-tri");
            this.setAttribute("data-tri", (nBoolDir=="0") ? "1" : "0");
            // Supprime la classe « selection » de chaque colonne.
            [].forEach.call( this.parentNode.querySelectorAll("th"), function(oTh) {oTh.classList.remove("selection");});
            // Ajoute la classe « selection » à la colonne cliquée.
            this.className = "selection";
            // Ajuste la flèche
            this.querySelector("span").className = (nBoolDir=="0") ? "flecheAsc" : "flecheDesc";

            // Construit la matrice
            // Récupère le tableau (tbody)
            var oTbody = this.parentNode.parentNode.parentNode.getElementsByTagName("tbody")[0];
            var oLigne = oTbody.rows;
            var nNbrLigne = oLigne.length;
            var aColonne = new Array(), i, j, oCel;
            for(i = 0; i < nNbrLigne; i++) {
                oCel = oLigne[i].cells;
                aColonne[i] = new Array();
                for(j = 0; j < oCel.length; j++){
                    aColonne[i][j] = oCel[j].innerHTML;
                }
            }

            // Trier la matrice (array)
            // Récupère le numéro de la colonne
            var nIndex = this.getAttribute("data-pos");
            // Récupère le type de tri (numérique ou par défaut « local »)
            var sFonctionTri = (this.getAttribute("data-type")=="num") ? "compareNombres" : "compareLocale";
            // Tri
            aColonne.sort(eval(sFonctionTri));
            // Tri numérique
            function compareNombres(a, b) {return a[nIndex-1] - b[nIndex-1];}
            // Tri local (pour support utf-8)
            function compareLocale(a, b) {return a[nIndex-1].localeCompare(b[nIndex-1]);}
            // Renverse la matrice dans le cas d’un tri descendant
            if (nBoolDir==0) aColonne.reverse();

            // Construit les colonne du nouveau tableau
            for(i = 0; i < nNbrLigne; i++){
                aColonne[i] = "<td>"+aColonne[i].join("</td><td>")+"</td>";
            }

            // assigne les lignes au tableau
            oTbody.innerHTML = "<tr>"+aColonne.join("</tr><tr>")+"</tr>";
        }
    </script>
    <div class="container">

        <!-- Title -->
        <div class="section-title">
            <h2>Classement Factions</h2>
            <div class="line color-1-bg"></div>
            <div class="test1"></div>

        </div>

        <!-- Diamond Divider -->
        <div class="hr-diamond width-50px">
            <span class="line"></span>
            <span class="diamond"></span>
        </div>
        <!-- /Diamond Divider -->

        <div class="alert alert-info">
            <h4>Informations</h4>
            <h5>Le classement s'actualise toutes les 2 heures.</h5>
            <!--  <p>Voir les <a class="alert-link" href="">Stats</a> </p>-->
        </div>



        <div class="container">
            <div class="row">
                <!-- Trigger the modal with a button -->
                <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Detail du classement</button>

                <!-- Modal -->
                <div id="myModal" class="modal fade" role="dialog">
                    <div class="vertical-alignment-modal">
                        <div class="modal-dialog vertical-align-center">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">×</button>
                                    <h4 class="modal-title">Classement faction </h4>
                                </div>

                                <div class="modal-body">

                                    <div class="row">
                                        <div class="table-responsive">
                                            <table class="table table-hover">

                                                <thead>
                                                <tr>
                                                <th>AP</th>
                                                <th>CLAIMS</th>
                                                <th>POWER MAX</th>
                                                <th>MONEY</th>
                                                <th>CTP</th>
                                                <th>RICHESSE</th>
                                                </thead>
                                                <td style="font-size:11px;">
                                                    1ap = 100 pts<br>
                                                    2ap = 250 pts<br>
                                                    3ap = 500 pts<br>
                                                    4ap = 750 pts<br>
                                                    5ap = 1000 pts<br>
                                                </td>
                                                <td style="font-size:11px;">
                                                    1-25 = 100 pts<br>
                                                    26-50 = 200 pts<br>
                                                    51-75 = 500 pts<br>
                                                    76-100 = 800 pts<br>
                                                    101-125 = 1000 pts<br>
                                                    126-150 = 1500 pts<br>
                                                    151-175 = 2000 pts<br>
                                                    176-190 = 3000 pts<br>
                                                    191-195 = 4000 pts<br>
                                                    196-200 = 5000 pts<br>
                                                </td>
                                                <td style="font-size:11px;">
                                                    1-25 = 10 pts<br>
                                                    26-50 = 50 pts<br>
                                                    51-75 = 100 pts<br>
                                                    76-100 = 200 pts<br>
                                                    101-125 = 300 pts<br>
                                                    126-150 = 500 pts<br>
                                                    151-175 = 750 pts<br>
                                                    176-190 = 1000 pts<br>
                                                    191-195 = 1500 pts<br>
                                                    196-200 = 2000 pts<br>
                                                </td>
                                                <td style="font-size:11px;">
                                                    1 / 10k  : 100 pts <br>
                                                    10k / 25k : 250 pts <br>
                                                    25k / 50 k : 500 pts <br>
                                                    50k / 70k : 750 pts <br>
                                                    70k / 100k : 1000 pts <br>
                                                    100k / 150k : 1500 pts <br>
                                                    150k / 200k : 2000 pts <br>
                                                    200k / 300k : 2250 pts <br>
                                                    300k / 400k : 2500 pts <br>
                                                    400k / 500k : 3000 pts <br>
                                                    500k / 750k : 3500 pts <br>
                                                    750 k / 1M : 4000 pts <br>
                                                    1m / 2m : 4500 pts <br>
                                                    2m / 4m : 5000 pts <br>
                                                    4m / 5m : 6000 pts <br>
                                                </td>
                                                <td style="font-size:11px;">

                                                </td>
                                                <td style="font-size:11px;">

                                                </td>

                                            </table>

                                        </div></div></div>







                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <div class="row">
            <div class="table-responsive">
                <table class="table table-hover avectri" >

                    <thead>
                        <tr class="success titreclassement">

                        <th data-tri="0">RANG</th>
                        <th data-tri="0" data-type="text">FACTIONS</th>
                        <th data-tri="0" data-type="num">AP</th>
                        <th data-tri="0" data-type="num">CLAIMS</th>
                        <th data-tri="0" data-type="num">POWER MAX</th>
                        <th data-tri="0" data-type="num">MONEY</th>
                        <th data-tri="0" >CTP</th>
                        <th data-tri="0" data-type="num">RICHESSE(pts)</th>
                        <th data-tri="0" class="selection" data-type="num">POINTS</th>
                        </thead>

                            </tr>

                        <tbody class="colorbody">

                        <?php
						//On charge directement la requete SQL depuis le fichier .sql pour éviter de spamer le php et le rendre plus propre.
						$templine = '';
						$lines = file('SQLRequestForClassement.sql');
						foreach ($lines as $line)
						{
							//On enleve les comm de la requete
							if (substr($line, 0, 2) == '--' || $line == '')
								continue;
							$templine .= $line;
						}
						//On lance la querry avec ce qu'on a lu du fichier
                        $requete = $bdd->prepare ($templine);
                        $requete->execute();
                      
						$rang=1;
                        while($resultats = $requete->fetch(PDO::FETCH_OBJ))
                        {
                            $id_bdd = $resultats->FactionUUID;
							
                            ?>

                        <tr>
                            <td><?php echo $rang; ?></td>
                            <td> <span class="label label-info nomfac"><?php echo  $resultats->FactionName;?></span></td>
                            <td><?php  echo $resultats->ClaimApNumber; ?></td>
                            <td><?php  echo $resultats->ClaimNumber; ?></td>
                            <td><?php echo $resultats->Power; ?></td>
                            <td><?php echo $resultats->Money; ?></td>
                            <td><?php echo 0 ?><i> (<?php echo 0 ?>s)</i></td>
                            <td><?php echo $resultats->Richesse; ?></td>
                            <td><?php echo $resultats->totalPoint; ?></td>


                        </tr>

                            <?php


						$rang++;
                            
                        }

                        ?>

                        </tbody>

                    </table>



                    <div class="clearfix"></div>
                    <ul class="pagination pull-right">
                        <li class="disabled"><a href="#"><span class="glyphicon glyphicon-chevron-left"></span></a></li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a></li>
                    </ul>



            </div>
        </div>

        <!-- Diamond Divider -->
        <div class="hr-diamond width-50p">
            <span class="line"></span>
            <span class="diamond"></span>
        </div>
        <!-- /Diamond Divider -->

    </div>





</section>

<div class="footer-copyright">
    <?php include '../jointures/footer.php'; ?>
</div>