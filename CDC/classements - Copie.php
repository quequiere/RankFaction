
<?php
session_start();
header('Content-type: text/html; charset=utf-8');

//---------------------------------------------------------
// Requires fichiers database
//---------------------------------------------------------
require_once('../configuration/configuration.php');
require_once('../configuration/configbravoure.php');
require_once('../configuration/baseDonnees.php');
require_once('../configuration/fonctions.php');

$titre = 'Classement faction';
require_once("../jointures/head.php");
require_once("../jointures/header.php");
?>
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet'  type='text/css'>
<style>
    /* Classe obligatoire pour les flèches */
    .flecheDesc {
        width: 0;
        height: 0;
        float:right;
        margin: 10px;
        border-left: 5px solid transparent;
        border-right: 5px solid transparent;
        border-bottom: 5px solid black;
    }
    .flecheAsc {
        width: 0;
        height: 0;
        float:right;
        margin: 10px;
        border-left: 5px solid transparent;
        border-right: 5px solid transparent;
        border-top: 5px solid black;
    }

    /* Classe optionnelle pour le style */



    .avectri th {text-align:center;padding:5px 0 0 5px;vertical-align: middle;background-color:#999690;color:#444;cursor:pointer;
        -webkit-touch-callout: none;
        -webkit-user-select: none;
        -khtml-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        -o-user-select: none;
        user-select: none;
    }

    .avectri th.selection {background-color:#5d625c;color: #e1741b;}
    .avectri th.selection .flecheDesc {border-bottom-color: #e1741b;}
    .avectri th.selection .flecheAsc {border-top-color: #e1741b;}


</style>

<section class="content-block default-bg">

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="http://getbootstrap.com/dist/js/bootstrap.min.js"></script>
    <script>
        // Tri dynamique de tableau HTML
        // Auteur : Copyright © 2015 - Django Blais
        // Source : http://trucsweb.com/tutoriels/Javascript/tableau-tri/
        // Sous licence du MIT.
        function twInitTableau() {
            // Initialise chaque tableau de classe « avectri »
            [].forEach.call( document.getElementsByClassName("avectri"), function(oTableau) {
                var oEntete = oTableau.getElementsByTagName("tr")[0];
                var nI = 1;
                // Ajoute à chaque entête (th) la capture du clic
                // Un picto flèche, et deux variable data-*
                // - Le sens du tri (0 ou 1)
                // - Le numéro de la colonne
                [].forEach.call( oEntete.querySelectorAll("th"), function(oTh) {
                    oTh.addEventListener("click", twTriTableau, false);
                    oTh.setAttribute("data-pos", nI);
                    if(oTh.getAttribute("data-tri")=="1") {
                        oTh.innerHTML += "<span class=\"flecheDesc\"></span>";
                    } else {
                        oTh.setAttribute("data-tri", "0");
                        oTh.innerHTML += "<span class=\"flecheAsc\"></span>";
                    }
                    // Tri par défaut
                    if (oTh.className=="selection") {
                        oTh.click();
                    }
                    nI++;
                });
            });
        }

        function twInit() {
            twInitTableau();
        }
        function twPret(maFonction) {
            if (document.readyState != "loading"){
                maFonction();
            } else {
                document.addEventListener("DOMContentLoaded", maFonction);
            }
        }
        twPret(twInit);

        function twTriTableau() {
            // Ajuste le tri
            var nBoolDir = this.getAttribute("data-tri");
            this.setAttribute("data-tri", (nBoolDir=="0") ? "1" : "0");
            // Supprime la classe « selection » de chaque colonne.
            [].forEach.call( this.parentNode.querySelectorAll("th"), function(oTh) {oTh.classList.remove("selection");});
            // Ajoute la classe « selection » à la colonne cliquée.
            this.className = "selection";
            // Ajuste la flèche
            this.querySelector("span").className = (nBoolDir=="0") ? "flecheAsc" : "flecheDesc";

            // Construit la matrice
            // Récupère le tableau (tbody)
            var oTbody = this.parentNode.parentNode.parentNode.getElementsByTagName("tbody")[0];
            var oLigne = oTbody.rows;
            var nNbrLigne = oLigne.length;
            var aColonne = new Array(), i, j, oCel;
            for(i = 0; i < nNbrLigne; i++) {
                oCel = oLigne[i].cells;
                aColonne[i] = new Array();
                for(j = 0; j < oCel.length; j++){
                    aColonne[i][j] = oCel[j].innerHTML;
                }
            }

            // Trier la matrice (array)
            // Récupère le numéro de la colonne
            var nIndex = this.getAttribute("data-pos");
            // Récupère le type de tri (numérique ou par défaut « local »)
            var sFonctionTri = (this.getAttribute("data-type")=="num") ? "compareNombres" : "compareLocale";
            // Tri
            aColonne.sort(eval(sFonctionTri));
            // Tri numérique
            function compareNombres(a, b) {return a[nIndex-1] - b[nIndex-1];}
            // Tri local (pour support utf-8)
            function compareLocale(a, b) {return a[nIndex-1].localeCompare(b[nIndex-1]);}
            // Renverse la matrice dans le cas d’un tri descendant
            if (nBoolDir==0) aColonne.reverse();

            // Construit les colonne du nouveau tableau
            for(i = 0; i < nNbrLigne; i++){
                aColonne[i] = "<td>"+aColonne[i].join("</td><td>")+"</td>";
            }

            // assigne les lignes au tableau
            oTbody.innerHTML = "<tr>"+aColonne.join("</tr><tr>")+"</tr>";
        }
    </script>
    <div class="container">

        <!-- Title -->
        <div class="section-title">
            <h2>Classement Factions</h2>
            <div class="line color-1-bg"></div>
            <div class="test1"></div>

        </div>

        <!-- Diamond Divider -->
        <div class="hr-diamond width-50px">
            <span class="line"></span>
            <span class="diamond"></span>
        </div>
        <!-- /Diamond Divider -->

        <div class="alert alert-info">
            <h4>Informations</h4>
            <h5>Le classement s'actualise toutes les 2 heures.</h5>
            <!--  <p>Voir les <a class="alert-link" href="">Stats</a> </p>-->
        </div>



        <div class="container">
            <div class="row">
                <!-- Trigger the modal with a button -->
                <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Detail du classement</button>

                <!-- Modal -->
                <div id="myModal" class="modal fade" role="dialog">
                    <div class="vertical-alignment-modal">
                        <div class="modal-dialog vertical-align-center">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">×</button>
                                    <h4 class="modal-title">Classement faction </h4>
                                </div>

                                <div class="modal-body">

                                    <div class="row">
                                        <div class="table-responsive">
                                            <table class="table table-hover">

                                                <thead>
                                                <tr>
                                                <th>AP</th>
                                                <th>CLAIMS</th>
                                                <th>POWER MAX</th>
                                                <th>MONEY</th>
                                                <th>CTP</th>
                                                <th>RICHESSE</th>
                                                </thead>
                                                <td style="font-size:11px;">
                                                    1ap = 100 pts<br>
                                                    2ap = 250 pts<br>
                                                    3ap = 500 pts<br>
                                                    4ap = 750 pts<br>
                                                    5ap = 1000 pts<br>
                                                </td>
                                                <td style="font-size:11px;">
                                                    1-25 = 100 pts<br>
                                                    26-50 = 200 pts<br>
                                                    51-75 = 500 pts<br>
                                                    76-100 = 800 pts<br>
                                                    101-125 = 1000 pts<br>
                                                    126-150 = 1500 pts<br>
                                                    151-175 = 2000 pts<br>
                                                    176-190 = 3000 pts<br>
                                                    191-195 = 4000 pts<br>
                                                    196-200 = 5000 pts<br>
                                                </td>
                                                <td style="font-size:11px;">
                                                    1-25 = 10 pts<br>
                                                    26-50 = 50 pts<br>
                                                    51-75 = 100 pts<br>
                                                    76-100 = 200 pts<br>
                                                    101-125 = 300 pts<br>
                                                    126-150 = 500 pts<br>
                                                    151-175 = 750 pts<br>
                                                    176-190 = 1000 pts<br>
                                                    191-195 = 1500 pts<br>
                                                    196-200 = 2000 pts<br>
                                                </td>
                                                <td style="font-size:11px;">
                                                    1 / 10k  : 100 pts <br>
                                                    10k / 25k : 250 pts <br>
                                                    25k / 50 k : 500 pts <br>
                                                    50k / 70k : 750 pts <br>
                                                    70k / 100k : 1000 pts <br>
                                                    100k / 150k : 1500 pts <br>
                                                    150k / 200k : 2000 pts <br>
                                                    200k / 300k : 2250 pts <br>
                                                    300k / 400k : 2500 pts <br>
                                                    400k / 500k : 3000 pts <br>
                                                    500k / 750k : 3500 pts <br>
                                                    750 k / 1M : 4000 pts <br>
                                                    1m / 2m : 4500 pts <br>
                                                    2m / 4m : 5000 pts <br>
                                                    4m / 5m : 6000 pts <br>
                                                </td>
                                                <td style="font-size:11px;">

                                                </td>
                                                <td style="font-size:11px;">

                                                </td>

                                            </table>

                                        </div></div></div>







                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <div class="row">
            <div class="table-responsive">
                <table class="table table-hover avectri" >

                    <thead>
                        <tr class="success titreclassement">

                        <th data-tri="0">RANG</th>
                        <th data-tri="0" data-type="text">FACTIONS</th>
                        <th data-tri="0" data-type="num">AP</th>
                        <th data-tri="0" data-type="num">CLAIMS</th>
                        <th data-tri="0" data-type="num">POWER MAX</th>
                        <th data-tri="0" data-type="num">MONEY</th>
                        <th data-tri="0" >CTP</th>
                        <th data-tri="0" data-type="num">RICHESSE(pts)</th>
                        <th data-tri="0" class="selection" data-type="num">POINTS</th>
                        </thead>

                            </tr>

                        <tbody class="colorbody">

                        <?php
                        $requete = $bdd->prepare ("SELECT * FROM aedenia_stats ORDER BY points DESC limit 15");
                        $requete->execute();
                        $i = 1;



                        while($resultats = $requete->fetch(PDO::FETCH_OBJ))
                        {



                            $id_bdd = $resultats->id;
                            $rang = $i;


                            $nom_fac = $resultats->nom_fac;
                            $nbr_ap = $resultats->nbr_ap;
                            $nbr_claims = $resultats->nbr_claims;
                            $power = $resultats->power;
                            $money_bank = $resultats->money_bank;
                            $event_ctp = $resultats->event_ctp;
                            $event_ctp_temps = $resultats->event_ctp_temps;
                            $richesse = $resultats->richesse;


                            $pts_ap = 0;
                            $pts_claims = 0;
                            $pts_power = 0;
                            $pts_money_bank = 0;
                            $pts_event_ctp = 0;
                            $pts_richesse = 0;
                            $pts_event_ctp_temps = 0;
                            /* POINTS AP */

                            if($nbr_ap==1)
                                $pts_ap = 100;
                            elseif ($nbr_ap==2)
                                $pts_ap = 250;
                            elseif ($nbr_ap==3)
                                $pts_ap = 500;
                            elseif ($nbr_ap==4)
                                $pts_ap = 750;
                            elseif ($nbr_ap==5)
                                $pts_ap = 1000;
                            else $pts_ap = 0;

                            /*****************/
                            /*****************/

                            /* POINTS CLAIMS */
                            if($nbr_claims >1  && $nbr_claims <= 25)
                                $pts_claims=100;

                            elseif ($nbr_claims >26  && $nbr_claims <= 50)
                                $pts_claims=200;

                            elseif ($nbr_claims >51  && $nbr_claims <= 75)
                                $pts_claims=500;

                            elseif ($nbr_claims >76  && $nbr_claims <= 100)
                                $pts_claims=800;

                            elseif ($nbr_claims >101  && $nbr_claims <= 125)
                                $pts_claims=1000;

                            elseif ($nbr_claims >126  && $nbr_claims <= 150)
                                $pts_claims=1500;

                            elseif ($nbr_claims >151  && $nbr_claims <= 175)
                                $pts_claims=2000;

                            elseif ($nbr_claims >176  && $nbr_claims <= 190)
                                $pts_claims=3000;

                            elseif ($nbr_claims >191  && $nbr_claims <= 195)
                                $pts_claims=4000;

                            elseif ($nbr_claims >196  && $nbr_claims <= 200)
                                $pts_claims=5000;

                            else  $pts_claims=0;



                            /*****************/
                            /*****************/

                            /* POINTS POWER */

                            if($power >1  && $power <= 25)
                                $pts_power=10;

                            elseif ($power >26  && $power <= 50)
                                $pts_power=50;

                            elseif ($power >51 && $power <= 75)
                                $pts_power=100;

                            elseif ($power >76  && $power <= 100)
                                $pts_power=200;

                            elseif ($power >101  && $power <= 125)
                                $pts_power=300;

                            elseif ($power >126  && $power <= 150)
                                $pts_power=500;

                            elseif ($power >151  && $power <= 175)
                                $pts_power=750;

                            elseif ($power >176  && $power <= 190)
                                $pts_power=1000;

                            elseif ($power >191 && $power <= 195)
                                $pts_power=1500;

                            elseif ($power >196  && $power <= 200)
                                $pts_power=2000;

                            else  $pts_power=0;

                            /*****************/
                            /*****************/

                            /* MONEY BANK */

                            if($money_bank >1  && $money_bank <= 10000)
                                $pts_money_bank=100;

                            elseif ($money_bank >10001  && $money_bank <= 25000)
                                $pts_money_bank=250;

                            elseif ($money_bank >25001  && $money_bank <= 50000)
                                $pts_money_bank=500;

                            elseif ($money_bank >50001  && $money_bank <= 70000)
                                $pts_money_bank=750;

                            elseif ($money_bank >70001  && $money_bank <= 100000)
                                $pts_money_bank=1000;

                            elseif ($money_bank >100001  && $money_bank <= 150000)
                                $pts_money_bank=1500;

                            elseif ($money_bank >150001 && $money_bank <= 200000)
                                $pts_money_bank=2000;

                            elseif ($money_bank >200001 && $money_bank <= 300000)
                                $pts_money_bank=2250;

                            elseif ($money_bank >300001 && $money_bank <= 400000)
                                $pts_money_bank=2500;

                            elseif ($money_bank >400001 && $money_bank <= 500000)
                                $pts_money_bank=3000;

                            elseif ($money_bank >500001 && $money_bank <= 750000)
                                $pts_money_bank=3500;

                            elseif ($money_bank >750001 && $money_bank <= 1000000)
                                $pts_money_bank=4000;

                            elseif ($money_bank >1000001 && $money_bank <= 2000000)
                                $pts_money_bank=4500;

                            elseif ($money_bank >2000001 && $money_bank <= 4000000)
                                $pts_money_bank=5000;

                            elseif ($money_bank >4000001 && $money_bank <= 5000000)
                                $pts_money_bank=6000;

                            else  $pts_money_bank=0;


                            /*****************/
                            /*****************/
                            $pts_rank = $pts_ap + $pts_claims + $pts_power + $pts_money_bank + $pts_event_ctp + $pts_event_ctp_temps + $pts_richesse;



                            $req = $bdd->prepare('UPDATE aedenia_stats SET points = :points WHERE id = :id');
                            $req->execute(array(
                                'points' => $pts_rank,
                                'id' => $id_bdd
                            ));

                            $req = $bdd->prepare('UPDATE aedenia_stats SET rang = :rang WHERE id = :id');
                            $req->execute(array(
                                'rang' => $rang,
                                'id' => $id_bdd
                            ));


                            ?>

                        <tr>
                            <td><?php echo $rang; ?></td>
                            <td> <span class="label label-info nomfac"><?php echo  $resultats->nom_fac;?></span></td>
                            <td><?php  echo $resultats->nbr_ap; ?></td>
                            <td><?php  echo $resultats->nbr_claims; ?></td>
                            <td><?php echo $resultats->power; ?></td>
                            <td><?php echo $resultats->money_bank; ?></td>
                            <td><?php echo $resultats->event_ctp; ?><i> (<?php echo $resultats->event_ctp_temps; ?>s)</i></td>
                            <td><?php echo $resultats->richesse; ?></td>
                            <td><?php echo $pts_rank; ?></td>


                        </tr>

                            <?php



                            $i++;
                        }

                        ?>

                        </tbody>

                    </table>



                    <div class="clearfix"></div>
                    <ul class="pagination pull-right">
                        <li class="disabled"><a href="#"><span class="glyphicon glyphicon-chevron-left"></span></a></li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a></li>
                    </ul>



            </div>
        </div>

        <!-- Diamond Divider -->
        <div class="hr-diamond width-50p">
            <span class="line"></span>
            <span class="diamond"></span>
        </div>
        <!-- /Diamond Divider -->

    </div>





</section>

<div class="footer-copyright">
    <?php include '../jointures/footer.php'; ?>
</div>